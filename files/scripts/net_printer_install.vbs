report =""

'функции

function isadmin()
	o=false
	
	isadmin=o
end function

'вырезаем из строки пробелы и табуляторы
function true_trim(str)
	str1=Replace (str,vbtab," ")
	true_trim=trim(str1)
'WScript.Echo true_trim	
end function

'функция создает порт принтера
Function set_printer_port(name, address) 
    Set objNewPort = objWMIService.Get _ 
        ("Win32_TCPIPPrinterPort").SpawnInstance_ 
    objNewPort.Name = name
    objNewPort.Protocol = 1  ' 1 = Raw, 2 = LPR 
    objNewPort.HostAddress = address
    objNewPort.PortNumber = "9100" 
    objNewPort.SNMPEnabled = False 
    objNewPort.Put_ 
    set_printer_port = name
end Function

'процедура установки принтера
sub set_printer (drivername,PortName, DeviceID, Location  )
    Set objPrinter = objWMIService.Get _ 
        ("Win32_Printer").SpawnInstance_ 
    objPrinter.DriverName = drivername 
    objPrinter.PortName   = PortName
    objPrinter.DeviceID   = DeviceID 
    objPrinter.Location = Location 
    objPrinter.Network = True 
    objPrinter.Shared = False 
    'objPrinter.ShareName = 
    objPrinter.Put_ 
end sub

sub mount_shared_printer(path)
'{
'	strComputer = "." 
	' Set objWMIService = GetObject("winmgmts:" _ 
	'   & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2") 
   
'	Dim objNetwork 
'	Set objNetwork = CreateObject("WScript.Network") 
'WScript.Echo path 
'	objNetwork.RemovePrinterConnection (path)  
'	objNetwork.AddWindowsPrinterConnection (path) 
'} это кусок требует полномочий пользователя, но кусок который дрова ставит не работает и на оборот

Set WshShell = CreateObject("WScript.Shell")
'Set WshExec = WshShell.Exec("rundll32 printui.dll,PrintUIEntry /ga /n "+chr(34)+path+chr(34))
rc = WshShell.Run("explorer.exe "+chr(34)+path+chr(34))
if rc =0 then
	report=report + "Добавлен принтер " + path + chr(13)
end if
'net stop spooler 
'net start spooler
end sub

'sub set_default_printer()
'    Set colInstalledPrinters =  objWMIService.ExecQuery _ 
'        ("Select * from Win32_Printer Where Name = 'Brother HL-1270N'") 
 
'    For Each objPrinter in colInstalledPrinters 
'        objPrinter.SetDefaultPrinter() 
'    next 
'end sub

'функция установки драйвера
Function setup_inf(t)
	DriverName=""
	DriverInf="\\admsrv\d\printers\drivers\"
	select case true_trim(t)
		Case "xeroxc405" 
		'точно работает
			DriverName = "Xerox VersaLink C405 PCL6" 
			DriverInf = DriverInf + "xerox\C405\VLC400_5.511.11.0_PCL6_x64_Driver.inf\x2NBOMX.inf"
				'lpr '515 'lp альтернативные настройки
		Case "hp_upd-pcl6"
			DriverName = "HP Universal Printing PCL 6"
			DriverInf = DriverInf + "hp\upd-pcl6-x64-6.6.0.23029\hpcu215u.inf"
		Case "hp400"
		'точно работает
			DriverName = "HP LaserJet 400 M401 PCL 6"
			DriverInf = DriverInf + "hp\LJ-M401-full-solution-15188\hpcm401u.inf"
		Case "hp1020"
			DriverName = "HP LaserJet 1020"
			DriverInf = DriverInf + "hp\LaserJet 1018 1020 1022\HPLJ1020.INF"
		Case "konica_universal"
			DriverName = "KONICA MINOLTA Universal PCL"
			DriverInf = DriverInf + "konica\universal_win_x64\KOBS7J__.inf"
		Case "konica20"
			DriverName = "KONICA MINOLTA bizhub C20 PCL6"
			DriverInf = DriverInf + "konica\c20\KODJOJ__.inf"
		Case "konica162"
			DriverName = "KONICA MINOLTA 162"
			DriverInf = DriverInf + "konica\162_180_210_BH162ALLWin7x64_2000EN\KM.inf"
		Case "konica180"
			DriverName = "KONICA MINOLTA 180"
			DriverInf = DriverInf + "konica\162_180_210_BH162ALLWin7x64_2000EN\KM.inf"
		Case "konica200_250_350"
			DriverName = "KONICA MINOLTA 350/250/200 VXL"
			DriverInf = DriverInf + "konica\350_250_200_PCLXLWin_x64_110BEU0RU\KOBZQLB_.inf"
		Case "konica210"
			DriverName = "KONICA MINOLTA 210"
			DriverInf = DriverInf + "konica\162_180_210_BH162ALLWin7x64_2000EN\KM.inf"
		Case "konica224e"
			DriverName = "KONICA MINOLTA C364SeriesPCL"
			DriverInf = DriverInf + "konica\224e_C364_C554\KOAYTJ__.INF"
		Case "konica287"
			DriverName = "KONICA MINOLTA C287SeriesPCL"
			DriverInf = DriverInf + "konica\konica\287_368_658_IT5PCL6Winx64_5130RU\KOAY8J__.INF"
		Case "konica360"
		'точно работает
			DriverName = "KONICA MINOLTA C360SeriesPCL"
			DriverInf = DriverInf + "konica\BHC360PCL6Winx86_3700RU\KOAZ8J__.INF"
		Case "konica364"
			DriverName = "KONICA MINOLTA C364SeriesPCL"
			DriverInf = DriverInf + "konica\224e_C364_C554\KOAYTJ__.INF"
		Case "konica368"
			DriverName = "KONICA MINOLTA C368SeriesPCL"
			DriverInf = DriverInf + "konica\konica\287_368_658_IT5PCL6Winx64_5130RU\KOAY8J__.INF"
		Case "konica554"
			DriverName = "KONICA MINOLTA C554SeriesPCL"
			DriverInf = DriverInf + "konica\224e_C364_C554\KOAYTJ__.INF"
		Case "konica658"
			DriverName = "KONICA MINOLTA C658SeriesPCL"
			DriverInf = DriverInf + "konica\konica\287_368_658_IT5PCL6Winx64_5130RU\KOAY8J__.INF"
		Case "konica3110"
		'точно работает
			DriverName = "KONICA MINOLTA C3110 PCL6"
			DriverInf = DriverInf + "konica\c3110 PCL6\KOBK4J__.inf"
		Case "konica4690" 
		'точно работает
			DriverName = "KONICA MINOLTA magicolor 4690MF"
			DriverInf = DriverInf + "konica\MC4690MFGDIWin7x64_2100RU\MXP___0F.INF"
		Case "konica7450"
			DriverName = "KONICA MINOLTA mc7450 PCL6"
			DriverInf = DriverInf + "konica\mc7450IIPCL6Win7x64_1110RU\KOBJ5J__.inf"

		Case "kyorocera-fs1020mfp"
			DriverName = "Kyocera FS-1020MFP GX"
			DriverInf = DriverInf + "kyorocera\fs1xxx\OEMSETUP.inf"
		Case "kyorocera-fs1025mfp"
			DriverName = "Kyocera FS-1025MFP GX"
			DriverInf = DriverInf + "kyorocera\fs1xxx\OEMSETUP.inf"
		Case "kyorocera-fs1040"
			DriverName = "Kyocera FS-1040 GX"
			DriverInf = DriverInf + "kyorocera\fs1xxx\OEMSETUP.inf"
		Case "kyorocera-fs1041"
			DriverName = "Kyocera FS-1041 GX"
			DriverInf = DriverInf + "kyorocera\fs1xxx\OEMSETUP.inf"
		Case "kyorocera-fs1060dn"
			DriverName = "Kyocera FS-1060DN GX"
			DriverInf = DriverInf + "kyorocera\fs1xxx\OEMSETUP.inf"
		Case "kyorocera-fs1061dn"
			DriverName = "Kyocera FS-1061DN GX"
			DriverInf = DriverInf + "kyorocera\fs1xxx\OEMSETUP.inf"
		Case "kyorocera-fs1120mfp"
			DriverName = "Kyocera FS-1120MFP GX"
			DriverInf = DriverInf + "kyorocera\fs1xxx\OEMSETUP.inf"
		Case "kyorocera-fs1125mfp"
			DriverName = "Kyocera FS-1125MFP GX"
			DriverInf = DriverInf + "kyorocera\fs1xxx\OEMSETUP.inf"
		Case "kyorocera-fs1220mfp"
			DriverName = "Kyocera FS-1220MFP GX"
			DriverInf = DriverInf + "kyorocera\fs1xxx\OEMSETUP.inf"
		Case "kyorocera-fs1320mfp"
			DriverName = "Kyocera FS-1320MFP GX"
			DriverInf = DriverInf + "kyorocera\fs1xxx\OEMSETUP.inf"
		Case "kyorocera-fs1325mfp"
			DriverName = "Kyocera FS-1325MFP GX"
			DriverInf = DriverInf + "kyorocera\fs1xxx\OEMSETUP.inf"

		Case "kyorocera-m2030dn"
			DriverName = "Kyocera ECOSYS M2030dn KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-m2035dn"
			DriverName = "Kyocera ECOSYS M2035dn KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-m2530dn"
			DriverName = "Kyocera ECOSYS M2530dn KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-m2535dn"
			DriverName = "Kyocera ECOSYS M2535dn KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-m6026cdn"
			DriverName = "Kyocera ECOSYS M6026cdn KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-m6026cdn-tb"
			DriverName = "Kyocera ECOSYS M6026cdn Type B KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-m6026cidn"
			DriverName = "Kyocera ECOSYS M6026cidn KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-m6026cidn-tb"
			DriverName = "Kyocera ECOSYS M6026cidn Type B KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-m6526cdn"
			DriverName = "Kyocera ECOSYS M6526cdn KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-m6526cidn"
			DriverName = "Kyocera ECOSYS M6526cidn KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-m6526cdn-tb"
			DriverName = "Kyocera ECOSYS M6526cdn Type B KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-m6526cidn-tb"
			DriverName = "Kyocera ECOSYS M6526cidn Type B KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
		Case "kyorocera-266ci"
			DriverName = "Kyocera TASKalfa 266ci KX"
			DriverInf = DriverInf + "kyorocera\mxxx\OEMSETUP.inf"
			
			
		Case "panasonic2061p" 'принтер
			DriverName = "Panasonic KX-MB2050 GDI"
			DriverInf = DriverInf + "panasonic\kx-mb2061\printer\IUJDA6.INF"

		Case "canon3525"
		'точно работает
			DriverName = "Canon Generic Plus PCL6"
			DriverInf = DriverInf + "canon\c3525\Cnp60MA64.INF"

	end select
	

	'если тип двайвера известен то устанавливаем
	if DriverName <>"" then
                                                                                         
	'установка inf
		' Establish WMI connection to specified computer. 
		' Note that the loaddriver privilege is required to add the driver. 
		Set WMI = GetObject("winmgmts:{impersonationlevel=impersonate" & ",(loaddriver)}!//" & "." & "/root/cimv2") 
 
		Set NewDriver = WMI.Get("Win32_PrinterDriver") 
		NewDriver.Name = DriverName 
		NewDriver.InfName = DriverInf 
		Result = NewDriver.AddPrinterDriver(NewDriver) 

		If Result = 0 Then 
            report=report + "Добавлен драйвер " + DriverName +chr(13)
		Else 
			WScript.Echo "Error " & Result & " adding printer driver: " & DriverName 
			WScript.Quit 
		End If 
	end if	
setup_inf = DriverName
end Function



''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' основной скрипт

' спросить номер кабинета 
kabinet=InputBox("Введите номер кабинета" + chr(13)+_
"пл:" +chr(13)+_
"110,115,125,127,134,134a,135"+ chr(13)+_
"201a,208,214,223,229,230,"+ chr(13)+_
"303,303a,307,309,320,322,326,329,330,332,335,344,345a"+chr(13)+_
"ул:"+chr(13)+_
"u8,u304" +chr(13)+_
"2а:"+chr(13)+_
"s9,s12,s3k -konica250 3 Этаж корридор"+chr(13)_
,"")




'On Error Resume Next 
 
'SETS 'LOAD DRIVER' PRIVILEGE. 
    Set objWMIService = GetObject("Winmgmts:") 
    objWMIService.Security_.Privileges.AddAsString "SeLoadDriverPrivilege", True 
'

'проходим по списку принтеров
	Set oShell = WScript.CreateObject("WScript.Shell")
 	strSourceFile = "\\admsrv\d\printers\scripts\printers_spisok.txt"

	set fso = CreateObject("Scripting.FileSystemObject")
	If fso.FileExists(strSourceFile) Then
		set file = fso.OpenTextFile(strSourceFile)
		Do Until file.AtEndOfStream
			strLine = file.ReadLine()
			strLine = true_trim (strLine)
			if strLine <> "" then
				A = Split(strLine,";")
				'A(0) кабинет
				s_kabinet=true_trim (A(0))
				'A(1) как назвать принтер
				vision_name_printer=true_trim(A(1))
				'A(2) тип принтера
				type_printer=true_trim(A(2))
				'A(3) адрес принтер
				net_adres=true_trim(A(3))
				
				if s_kabinet = kabinet then
					' проверяем сетевой принтер или расшаренный с компьютера
					if InStr(net_adres,"\\") > 0 then
						mount_shared_printer(net_adres)
					Else
						' устанавливаем inf в базу
						driver_name = setup_inf(type_printer)

						port_name =set_printer_port(type_printer+"_"+kabinet, net_adres) 
						set_printer driver_name,port_name, vision_name_printer, kabinet
                        report=report + "Установлен "+ vision_name_printer+chr(13)
					end if
				end if
			end if
		Loop
	end if	

if report ="" then report="Не выполнено ничего"
WScript.Echo report
